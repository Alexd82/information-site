$(document).ready(function(){
    $(document).on('click', '.ajaxCall', function(){       
       
       var  location        = $(this).attr('data-href'),
            origin_content  = "<div class = 'news_overview' style = 'display: none;'>" + $(this).parents('.news_wrapper').html() + "</div>",
            button          = "<div class = 'news_back_button'>Back</div>";
       
       $('.news_wrapper').fadeOut(700, function(){
           $.ajax({
    			type:       'POST',
    			data:       '',
    			url:        'blogs/' + location,
    			async:      false,
    			success:    function(response){
    				$('.news_wrapper').html(button + response + button + origin_content);
    				$('.news_wrapper').fadeIn(400);		
    			}				
    		});
       });
       
    });
    
    $(document).on('click', '.news_back_button', function(){
        $('.news_wrapper').fadeOut(700, function(){
            
            var origin_content  = $('.news_overview').html();
            
            $('.news_wrapper').html(origin_content);
            $('.news_wrapper').fadeIn(400);
        });
    });    
});