$(document).ready(function(){
	var stickyTop 			= $('#header').offset().top,
		stickyHeader		= function(){
			var scrollTop	= $(window).scrollTop();
			$('#header').wrap('<div class="header_placeholder"></div>');
			$('.header_placeholder').height($('#header').outerHeight());

			if(scrollTop > stickyTop){
				$('#header').addClass('sticky');
			} else {
				$('#header').removeClass('sticky');
			}
		},
        docHeight  			= $(window).height();

    $('#header').wrap('<div class="header_placeholder"></div>');
    $('.header_placeholder').height($('#header').outerHeight());
	
	stickyHeader();

	$(window).scroll(function(){
		stickyHeader();
	});
});