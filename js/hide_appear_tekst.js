//------  Hide text and appear after scrolling -----//
	$(document).ready(function(){
		var aboutEl = $('div.about'),
			aboutElOffset = aboutEl.offset().top/1.6,
			documentEl = $(document);

			documentEl.on('scroll', function(){
				if (documentEl.scrollTop() > aboutElOffset && aboutEl.hasClass('hidden') ) aboutEl.removeClass('hidden');
			});
	});
	