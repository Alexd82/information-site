$(document).ready(function(){
	//hide all the images except the first one
	$("#gallery img:gt(0)").hide();

	setInterval(function(){
		//get the current image - it is the visible one
		var current = $('#gallery img:visible');

		//get immediate next image after the current if excists,
		//otherwise find the first one
		var next = current.next().length ? current.next() : $('#gallery img:eq(0)');
		//hide the current image
		current.fadeOut(1000);
		//show the next one
		next.fadeIn(1000);
	}, 3000);


})

